
/* 1. Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
   
 Рекурсия - это когда функция внутри своего тела вызывает сама себя. 
 С помощью рекурсии мы можем повторять вычисления пока не будет достигнуто необходимое условие, так же как в циклах.   
*/



// Все способы считают числа больше чем 2^53

const page = document.querySelector(".main");
let number = setNumber(`Введіть натуральне число`, 10);

function setNumber(text, value) {
   let num = +prompt(text, value);
   while (!Number.isInteger(num) || num < 1) {
      alert(`Ви ввели не корректні данні!`);
      num = +prompt("Повторіть ввід натурального числа", num);
   }
   return BigInt(num) * 1n;
}

function factorialRecursion(num) {
   return num > 1n ? num * factorialRecursion(num - 1n) : 1n;
}

function factorialCycleFor(num) {
   let result = 1n;
   for (let i = num; i > 0; i--) {
      result *= i;
   }
   return result;
}

function factorialArrMethods(num) {
   return Array(Number(num)).fill(1n).map((el, i) => el += BigInt(i)).reduce((a, b) => a * b);
}

const variant_1 = `Метод рекрсии: ${number}! = ${factorialRecursion(number)}`;
const variant_2 = `С помощью цикла for: ${number}! = ${factorialCycleFor(number)}`;
const variant_3 = `С помощью методов массива: ${number}! = ${factorialArrMethods(number)}`;

page.insertAdjacentHTML('beforeend', `<p>${variant_1}</p>`);
page.insertAdjacentHTML('beforeend', `<p>${variant_2}</p>`);
page.insertAdjacentHTML('beforeend', `<p>${variant_3}</p>`);